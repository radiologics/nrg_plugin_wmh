package org.nrg.xnat.turbine.modules.screens;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.nrg.xdat.XDAT;
import org.nrg.xdat.model.XnatAbstractresourceI;
import org.nrg.xdat.model.XnatSubjectassessordataI;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatResourcecatalog;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTItem;
import org.nrg.xft.search.ItemSearch;

import java.util.*;


@SuppressWarnings("unused")
public class PipelineScreen_WMH extends DefaultPipelineScreen  {

    public final String T1_CONFIG_TOOL_NAME = "params";
    public final String T1_CONFIG_FILE_NAME = "mr_scan_types";
    public final String FLAIR_CONFIG_TOOL_NAME = "params";
    public final String FLAIR_CONFIG_FILE_NAME = "flair_scan_types";

    public final ConfigService configService = XDAT.getConfigService();


    private ObjectMapper mapper = new ObjectMapper();
    private List<String> mrScanTypeList;

    public void finalProcessing(RunData data, Context context) {
        // HashMap<String,Object> objectsWeNeed = Maps.newHashMap();
        HashSet<XnatImagescandata> structuralScansSet = Sets.newHashSet();
        XnatImagesessiondata session = (XnatImagesessiondata) om;


        // String mrScantypeCsv = getConfig(SCANTYPE_CONFIG_TOOL_NAME, SCANTYPE_CONFIG_FILE_NAME);
        ArrayList<XnatImagescandata> t1Scans = getScansFromScantypeConfig(T1_CONFIG_TOOL_NAME, T1_CONFIG_FILE_NAME);
        if (t1Scans.size() == 0) {
            data.setMessage("ERROR Could not find any T1 scans in session " + session.getLabel());
            return;
        } else {
            context.put("t1Scans", t1Scans);
            // structuralScansSet.addAll(t1Scans);
        }

        // Get FLAIRs on this session
        Map<XnatImagesessiondata,List<XnatImagescandata>> flairScanMap = Maps.newLinkedHashMap();
        ArrayList<XnatImagescandata> flairScans = getScansFromScantypeConfig(FLAIR_CONFIG_TOOL_NAME, FLAIR_CONFIG_FILE_NAME);
        flairScanMap.put(session, flairScans);

        // Get FLAIRs on any other session attached to the subject
        String flairScantypeCsv;
        try {
            flairScantypeCsv = getConfig(FLAIR_CONFIG_TOOL_NAME, FLAIR_CONFIG_FILE_NAME, session.getProject());
        } catch (Exception e) {
            data.setMessage("ERROR Could not read FLAIR scan types.\n"+e.getMessage());
            return;
        }

        XFTItem subjSearch;
        try {
            subjSearch = ItemSearch.GetItem("xnat:subjectData.ID", session.getSubjectId(), TurbineUtils.getUser(data), false);
        } catch (Exception e) {
            data.setMessage("ERROR Could not search subject for FLAIR scans.\n"+e.getMessage());
            return;
        }
        XnatSubjectdata subject = new XnatSubjectdata(subjSearch);
        for (XnatSubjectassessordataI expt : subject.getExperiments_experiment()) {
            if (!(expt instanceof XnatImagesessiondata)) {
                continue;
            }

            if (StringUtils.equals(expt.getId(), session.getId())) {
                continue;
            }

            XnatImagesessiondata someSubjectSession;
            try {
                someSubjectSession = (XnatImagesessiondata)expt;
            } catch (ClassCastException e) {
                continue;
            }

            List<XnatImagescandata> flairs = someSubjectSession.getScansByTypeCsv(flairScantypeCsv);
            if (flairs == null || flairs.size() == 0) {
                continue;
            }

            flairScanMap.put(someSubjectSession, flairs);
            // structuralScansSet.addAll(flairs);

        }
        if (flairScanMap.size() == 0) {
            data.setMessage("ERROR Could not find any FLAIR scans in any session associated with subject" + subject.getLabel());
            return;
        }
        context.put("flairScanMap", flairScanMap);

        HashMap<String,ArrayList<String>> allScanResourceLabels = Maps.newHashMap();
        HashSet<String> scanResourceLabelSet = Sets.newHashSet();
        for (XnatImagescandata scan : t1Scans) {
            ArrayList<String> scanResourceLabels = Lists.newArrayList();
            for ( XnatAbstractresourceI scanResource : scan.getFile() ) {
                if (scanResource instanceof XnatResourcecatalog) {
                    String scanResourceLabel = scanResource.getLabel();
                    scanResourceLabels.add(StringUtils.isNotBlank(scanResourceLabel) ? scanResourceLabel : "null");
                }
            }
            allScanResourceLabels.put("t1-"+scan.getId(),scanResourceLabels);

            scanResourceLabelSet.addAll(scanResourceLabels);
        }
        for (XnatImagesessiondata flairSession : flairScanMap.keySet()) {
            for (XnatImagescandata scan : flairScanMap.get(flairSession)) {
                ArrayList<String> scanResourceLabels = Lists.newArrayList();
                for (XnatAbstractresourceI scanResource : scan.getFile()) {
                    if (scanResource instanceof XnatResourcecatalog) {
                        String scanResourceLabel = scanResource.getLabel();
                        scanResourceLabels.add(StringUtils.isNotBlank(scanResourceLabel) ? scanResourceLabel : "null");
                    }
                }
                allScanResourceLabels.put(flairSession.getId() + "-" + scan.getId(), scanResourceLabels);

                scanResourceLabelSet.addAll(scanResourceLabels);
            }
        }
        if (scanResourceLabelSet.contains("null")) {
            scanResourceLabelSet.remove("null");
        }
        context.put("allScanResourceLabels", allScanResourceLabels);
        context.put("scanResourceLabelSet", scanResourceLabelSet);


        Boolean hasNifti = scanResourceLabelSet.contains("NIFTI");
        context.put("hasNifti", hasNifti.toString());


        context.put("projectParameters", projectParameters);
    }

    public String getConfig(String configToolName, String configFileName, String project) throws Exception {
        Configuration config = configService.getConfig(configToolName, configFileName, Scope.Project, project);
        if (config == null || config.getContents() == null) {
            throw new Exception("Cannot find config at /projects/"+project+"/config/"+configToolName+"/"+configFileName);
        }
        return config.getContents().trim();
    }

}
