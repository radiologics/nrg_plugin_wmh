package org.nrg.xnat.wmh;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_wmh", name = "XNAT 1.7 White Matter Hypointensities Plugin", description = "This is the XNAT 1.7 White Matter Hypointensities Plugin.",
        dataModels = {@XnatDataModel(value = "wmh:wmhData",
                singular = "WMH",
                plural = "WMHs")})
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class WhiteMatterHypointensitiesPlugin {
}